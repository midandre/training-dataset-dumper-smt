/*
   Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
 */

/*

A tool to run jet systematic tool before retagging.
Authorr; Bingxuan Liu (bingxuan.liu@cern.ch)

Basic structures and functions are inspired by xAODAnaHelper and AnalysisTop.

*/

#include "JetSystematicsAlg.h"

#include "xAODJet/JetAuxContainer.h"
#include "AsgMessaging/MessageCheck.h"

JetSystematicsAlg::JetSystematicsAlg(const std::string& name, ISvcLocator *pSvcLocator):
  AthReentrantAlgorithm(name, pSvcLocator),
  m_calibration_tool("JetCalibrationTool"),
  m_uncertainties_tool("JetUncertaintiesTool"),
  m_Sigma(1.0)
{
  declareProperty( "systematic_variations", m_SystNames);
  declareProperty( "jet_calib_tool", m_calibration_tool);
  declareProperty( "jet_uncert_tool", m_uncertainties_tool);
  declareProperty( "sigma", m_Sigma);
}

JetSystematicsAlg::~JetSystematicsAlg() = default;

StatusCode JetSystematicsAlg::initialize() {

  ATH_MSG_INFO( "Initializing JetSystematicAlg Interface... ");

  ATH_CHECK( m_inputContainer.initialize() );
  ATH_CHECK( m_outContainerKey.initialize() );
  ATH_CHECK( m_calibration_tool.retrieve() );
  ATH_CHECK( m_uncertainties_tool.retrieve() );
  //
  // Get a list of recommended systematics for this tool
  //
  for (const auto& name : m_SystNames) {

    ATH_MSG_INFO("Adding systematic variation "<< name);
    // Bing: Need to fix this as the up/down variations need to be setup separately
    m_ActiveSysts.insert(CP::SystematicVariation(name, m_Sigma));
  }

  if ( m_uncertainties_tool->applySystematicVariation(m_ActiveSysts) != StatusCode::SUCCESS ) {
    ATH_MSG_ERROR( "Cannot configure JetUncertaintiesTool for systematic " << m_SystNames);
    return StatusCode::FAILURE;
  }

  ATH_MSG_INFO( "JetCalibrator Interface succesfully initialized!" );

  return StatusCode::SUCCESS;

}

StatusCode JetSystematicsAlg::execute(const EventContext& ctx) const
{
  SG::ReadHandle jets(m_inputContainer, ctx);

  // Perform nominal calibration
  SG::WriteHandle goodJets(m_outContainerKey, ctx);
  ATH_CHECK(goodJets.record(std::make_unique<xAOD::JetContainer>(),
                            std::make_unique<xAOD::JetAuxContainer>()));

  // we should probably use a shallow copy here
  for (const xAOD::Jet* jet : *jets) {
    *goodJets->emplace_back(new xAOD::Jet()) = *jet;
  }

  for ( auto jet_itr : *goodJets ) {

    // b-jet truth labelling
    //
    // decide whether or not the jet is a b-jet (truth-labelling +
    // kinematic selections) parton labeling scheme is used as this is
    // the one used to derive the calibration for bjets. See:
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetUncertaintiesRel21Summer2018SmallR#JES_Precision_Reduced_Flavour_Un
    static SG::AuxElement::ConstAccessor<int> PartonTruthLabelID ("PartonTruthLabelID");

    static SG::AuxElement::Decorator<char> accIsBjet("IsBjet"); // char due to limitations of ROOT I/O, still treat it as a bool
    accIsBjet(*jet_itr) = (PartonTruthLabelID( *jet_itr ) == 5 ? 1: 0);
  }//for jets

  // Apply the calibration
  ATH_CHECK(m_calibration_tool->applyCalibration(*goodJets));

  // apply the systematic variation
  ANA_CHECK(m_uncertainties_tool->applyContainerCorrection(*goodJets));
  return StatusCode::SUCCESS;
}

StatusCode JetSystematicsAlg::finalize(){
  return StatusCode::SUCCESS;
}
